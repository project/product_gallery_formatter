<?php

/**
 * @file
 * Contains \Drupal\product_gallery_formatter\Plugin\Field\FieldFormatter\ProductGalleryFormatter.
 */

namespace Drupal\product_gallery_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'product_gallery_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "product_gallery_formatter",
 *   label = @Translation("Product Gallery Formatter"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ProductGalleryFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items,  $langcode) {
    // Skip if there are no items to display.
    if ($items->isEmpty()) {
      return [];
    }

    // Retrieve file IDs.
    $fids = [];
    foreach ($items->getIterator() AS $item) {
      $fids[] = $item->get('target_id')->getValue();
    }

    // Load files so we can get their URIs.
    $files = \Drupal::EntityTypeManager()->getStorage('file')->loadMultiple($fids);

    // Make a list of all URIs.
    $uris = array_map(function($file) {
      return $file->getFileUri();
    }, $files);

    // Set up the render element.
    $element = [
      '#attached' => [
        'library' => [
          'product_gallery_formatter/product_gallery_formatter'
        ]
      ]
    ];



    // Create the main image carousel.
    $element['carousel'] = [
      '#theme' => 'product_image_list',
      '#items' => [],
      '#weight' => 1,
      '#attributes' => [
        'class' => [
          'sp-wrap',
        ],
      ]
    ];

    foreach ($uris AS $uri) {
      $element['carousel']['#items'][] = $this->getImageItem($uri, $this->getSetting('image_style'));
    }

    return [$element];
  }



  /**
   * Helper method to generate image for provided style
   * that takes the lazy load setting into account.
   *
   * @param string $uri
   *   The original image uri.
   * @param string $image_style
   *   The image style name.
   * @param bool $lazy_load
   *   If TRUE the lazy slick load image will be returned.
   *
   * @return array
   *   Renderable array for a single image.
   */
  function getImageItem($uri, $image_style, $lazy_load = FALSE) {
    $style = \Drupal::EntityTypeManager()
      ->getStorage('image_style')
      ->load($image_style);
    return [
      '#type' => 'inline_template',
      '#template' => '<a href="{{ path }}"><img src="{{ path }}" /></a>',
      '#context' => [
        'path' => $style ? $style->buildUrl($uri) : file_create_url($uri)
      ]
    ];
  }

}
